package com.simandsam.android.app;

import android.content.Context;
import android.content.SharedPreferences;

public class CommonPreference {

    private static final String APPPREF = "AppPrefs";
    private static final String USERPREF = "UserPrefs";

    private static SharedPreferences apppreferences;
    private static SharedPreferences userpreferences;

    public static String FCMTOKEN = "fcmtoken";
    public static String loggedIn = "loggedIn";

    public static void setAppPreference(Context context, String key, String value) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = apppreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }
    public static void setAppPreference(Context context, String key, Integer value) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = apppreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void setAppPreference(Context context, String key, boolean value) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = apppreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getAppPreferenceString(Context context, String key) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        return apppreferences.getString(key, "");
    }
    public static int getAppPreferenceInt(Context context, String key) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        return apppreferences.getInt(key, 0);
    }

    public static boolean getAppPreferenceBoolean(Context context, String key) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        return apppreferences.getBoolean(key, false);
    }


    public static void removeAppPreference(Context context, String key) {
        apppreferences = context.getSharedPreferences(APPPREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = apppreferences.edit();
        editor.remove(key);
        editor.apply();
    }


}
