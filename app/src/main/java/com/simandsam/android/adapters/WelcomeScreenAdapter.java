package com.simandsam.android.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WelcomeScreenAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private int[] welcomeScreen;
    private Context context;

    public WelcomeScreenAdapter() {
    }

    public WelcomeScreenAdapter(int[] welcomeScreen, Context context) {

        this.welcomeScreen = welcomeScreen;
        this.context = context;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(layoutInflater != null) {
            View view = layoutInflater.inflate(welcomeScreen[position], container, false);
            container.addView(view);

            return view;
        }else {
            return container.getRootView();
        }
    }

    @Override
    public int getCount() {
        return welcomeScreen.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
