package com.simandsam.android.utils;

import android.app.Activity;
import android.content.Intent;

import com.simandsam.android.R;

public class AppHelper {


    public static void LaunchActivity(Activity mContext, Class mActivity) {
        Intent intent = new Intent(mContext,mActivity);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);

    }

}
