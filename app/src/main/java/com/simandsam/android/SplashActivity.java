package com.simandsam.android;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.simandsam.android.activities.MainActivity;
import com.simandsam.android.activities.WelcomeActivity;
import com.simandsam.android.app.CommonPreference;
import com.simandsam.android.utils.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.splash_iv)
    ImageView ivSplash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        ivSplash.setImageResource(R.drawable.simsam);
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        ivSplash.setAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(CommonPreference.getAppPreferenceBoolean(getApplicationContext(),CommonPreference.loggedIn)){
                    AppHelper.LaunchActivity(SplashActivity.this, MainActivity.class);
                    finish();
                }else{
                    AppHelper.LaunchActivity(SplashActivity.this, WelcomeActivity.class);
                    finish();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
