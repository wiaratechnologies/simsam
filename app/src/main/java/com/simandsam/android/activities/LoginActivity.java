package com.simandsam.android.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.simandsam.android.R;
import com.simandsam.android.app.CommonPreference;
import com.simandsam.android.utils.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.loginEtNumber)
    EditText etMobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        etMobileNumber.addTextChangedListener(textWatcher);

    }


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text_value = etMobileNumber.getText().toString().trim();
            if (text_value.equalsIgnoreCase("+91")) {
                etMobileNumber.setText("");
            } else {
                if (!text_value.startsWith("+91") && text_value.length() > 0) {
                    etMobileNumber.setText("+91" + s.toString());
                    Selection.setSelection(etMobileNumber.getText(), etMobileNumber.getText().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void goToMain() {

        AppHelper.LaunchActivity(this,MainActivity.class);
        CommonPreference.setAppPreference(getApplicationContext(),CommonPreference.loggedIn,true);
        finish();

    }

    @OnClick(R.id.loginDone)
    public void onViewClicked() {
        if(etMobileNumber.getText().length()==13){
            goToMain();
        }else{
            validMobileAlert();
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Valid Mobile Number??");
            alertDialog.setMessage("Enter Valid Mobile Number");
            alertDialog.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }

    private void validMobileAlert() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Valid Mobile Number??");
        mBuilder.setMessage("Enter Valid Mobile Number");
        mBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mBuilder.setCancelable(false);
        AlertDialog alertDialog = mBuilder.create();
        alertDialog.show();

        // Getting the view elements
        TextView textView = (TextView) alertDialog.getWindow().findViewById(android.R.id.message);
        TextView alertTitle = (TextView) alertDialog.getWindow().findViewById(R.id.alertTitle);
        Button button2 = (Button) alertDialog.getWindow().findViewById(android.R.id.button2);

// Setting font
        textView.setTypeface(ResourcesCompat.getFont(this,R.font.abhaya_libre_medium));
        alertTitle.setTypeface(ResourcesCompat.getFont(this,R.font.abhaya_libre_extrabold));
        button2.setTypeface(ResourcesCompat.getFont(this,R.font.abhaya_libre));
    }
}
