package com.simandsam.android.activities;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.simandsam.android.R;
import com.simandsam.android.adapters.WelcomeScreenAdapter;
import com.simandsam.android.utils.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeActivity extends AppCompatActivity {

    private static final String TAG = "WelcomeActivity";

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.layoutDots)
    LinearLayout dotsLayout;

    TextView[] pages;
    int[] welcomeScreen;

    @BindView(R.id.skip)
    TextView skip;
    @BindView(R.id.done)
    TextView done;


    private WelcomeScreenAdapter welcomeScreenAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        initUI();

        initActions();
    }



    private void initUI() {
        welcomeScreen = new int[]{
                R.layout.wizzard_intro_1,
                R.layout.wizzard_intro_2,
                R.layout.wizzard_intro_3,
        };

        addPagination(0);

    }

    private void addPagination(int currentPage) {
        pages = new TextView[welcomeScreen.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < pages.length; i++) {
            pages[i] = new TextView(this);
            pages[i].setText(Html.fromHtml("&#8226;"));
            pages[i].setTextSize(35);
            pages[i].setTextColor(getResources().getColor(R.color.md_grey_300));
            dotsLayout.addView(pages[i]);
        }

        if (pages.length > 0) {
            pages[currentPage].setTextColor(getResources().getColor(R.color.md_orange_900));
        }
    }

    private void initActions() {
        ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                addPagination(position);


                if (position == welcomeScreen.length - 1) {
                    done.setText("Done");
                    skip.setVisibility(View.GONE);
                } else {
                    done.setText("Next");
                    skip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        };


        welcomeScreenAdapter = new WelcomeScreenAdapter(welcomeScreen, getApplicationContext());
        mViewPager.setAdapter(welcomeScreenAdapter);
        mViewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchHomeScreen();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current = getItem();
                if (current < welcomeScreen.length) {
                    mViewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });

    }
    private int getItem() {
        return mViewPager.getCurrentItem() + 1;
    }

    private void launchHomeScreen() {
        AppHelper.LaunchActivity(WelcomeActivity.this,LoginActivity.class);
        finish();

    }

}
